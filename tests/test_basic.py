import pytest

from temp_example_of_lspci_parsing.app import build_app


@pytest.mark.asyncio
async def test_basic():
    """Test basic functionality."""
    # TODO
    app = build_app()
    assert len(await app.execute()) > 1
