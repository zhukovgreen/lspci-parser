import asyncio
import re

from asyncio.subprocess import PIPE, Process
from typing import Tuple

from envparse import env
from loguru import logger

from .pkg_utils import get_title_description, get_version
from .setup_logger import setup_logger


env.read_envfile()
DEBUG = env.bool("DEBUG", default=False)


class PCIParser:
    def __init__(self) -> None:
        logger.debug("PCIParser loaded")
        self.device_id_re = re.compile("(?:Vendor|Device):\t(.+)")
        self.container_keys = ("Vendor", "Device", "SVendor", "SDevice")

    async def execute(self) -> Tuple[dict]:
        process: Process = await asyncio.create_subprocess_exec(
            "lspci", "-vmmkn", stdout=PIPE, stderr=PIPE
        )
        stdout, stderr = await process.communicate()
        stdout, stderr = tuple(map(bytes.decode, (stdout, stderr)))
        return tuple(
            i
            for i in map(
                lambda pci_ids: dict(
                    zip(
                        self.container_keys, self.device_id_re.findall(pci_ids)
                    )
                ),
                stdout.split("Slot"),
            )
            if i
        )


def build_app(debug=False) -> PCIParser:
    """Build the application."""
    version = get_version()
    title = get_title_description()
    setup_logger(debug=debug or DEBUG)
    logger.debug("Welcome to the application {}. {}".format(*title))
    logger.debug(f"Building application version {version} started...")
    # TODO create app instance
    app = PCIParser()
    return app
