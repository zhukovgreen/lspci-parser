import asyncio

from pprint import pprint

import click

from temp_example_of_lspci_parsing.app import build_app


@click.command()
@click.option("-v", "--verbose", count=True, help="Verbosity of the tool")
def cli(verbose: bool) -> None:
    """Cli tool to parse lspci"""
    # TODO Adjust the docstring
    app = build_app(debug=verbose)
    click.echo(pprint(asyncio.run(app.execute())), color="green")
